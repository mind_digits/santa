from sql import *
from classes import *
from settings import *
import uuid
from yookassa import Configuration, Payment, domain
from datetime import datetime, timedelta


def create_payment(amount, uid, gid, plan):
    Configuration.account_id = shop_id
    Configuration.secret_key = token_yookassa
    from datetime import datetime, timedelta
    td = timedelta(hours=1)
    exp_time = datetime.utcnow() + td
    payment = Payment.create({
        "amount": {
            "value": amount,
            "currency": "RUB"
        },
        "confirmation": {
            "type": "redirect",
            "return_url": "https://t.me/tayniysantabot"
        },
        "capture": True,
        "description": "%s=%s=%s" % (uid, gid, plan),
        "expires_at": exp_time
    }, uuid.uuid4())
    link = payment.confirmation.confirmation_url
    # bot.send_message(chat_id=uid, text="Ссылка для оплаты\n%s" % link)
    addpayment(payment.id, gid, uid, plan, datetime.utcnow().hour)
    return link



def sendcheck(bot, update, args):
    uid = update.message.from_user.id
    who_paid_uid = args[0]
    if len(args) < 3:
        bot.send_message(chat_id=uid, text='Плохие аргументы. Юзерайди, сумма, ссылка на чек.')
        return
    try:
        amount = int(args[1])
    except ValueError:
        bot.send_message(chat_id=uid, text='Плохие аргументы. Юзерайди, сумма, ссылка на чек.')
        return
    if str(uid) in admin_list:
        checkid = args[2]
        amount = args[1]
        if addcheck(who_paid_uid, amount, checkid):
            text = 'Спасибо за использование бота!\nВот ваш чек :)\n%s' % args[2]
            bot.send_message(chat_id=who_paid_uid, text=text)
        else:
            text = 'Не нашел совпадения пользователя и суммы'
            bot.send_message(chat_id=uid, text=text)


def ch2(bot, update, args):
    uid = update.message.from_user.id
    if len(args) < 3:
        bot.send_message(chat_id=uid, text='Плохие аргументы. Юзерайди, группа, сумма, ссылка на чек.')
        return
    ch2_sql(args[0], args[1], args[2], args[3])
    text = 'Спасибо за использование бота!\nВот ваш чек :)\n%s' % args[2]
    bot.send_message(chat_id=args[0], text=text)




def pay(bot, update):
    uid = update.message.chat_id

    Configuration.account_id = shop_id
    Configuration.secret_key = token_yookassa

    td = timedelta(hours=1)
    exp_time = datetime.utcnow() + td
    payment = Payment.create({
        "amount": {
            "value": "10.00",
            "currency": "RUB"
        },
        "confirmation": {
            "type": "redirect",
            "return_url": "https://t.me/tayniysantabot"
        },
        "capture": True,
        "description": uid,

        "expires_at": exp_time
    }, uuid.uuid4())
    link = payment.confirmation.confirmation_url
    bot.send_message(chat_id=uid, text="Ссылка для оплаты\n%s" % link)
    # addpayment(payment.id, 123123, uid)


def do_tick(bot, smth):
    try:
        payments = get_all_payments()
        # print(len(payments), payments.__sizeof__())
        payments.__sizeof__()
        Configuration.account_id = shop_id
        Configuration.secret_key = token_yookassa
        for p in payments:
            try:
                p_obj = Payment.find_one(p[0])
                if p_obj.status == 'canceled':
                    pass
                    # print(p_obj.status, p)
                    # bot.send_message(chat_id=admin_list[0], text=f"{p}\n{p_obj.cancellation_details.reason}")
                if p_obj.status == 'succeeded':
                    am = str(p_obj.amount.value)
                    if p[3] != '0':
                        update_plan(p[1], p[3])
                    else:
                        bot.send_message(chat_id=admin_list[0], text=f"💰 {am}")
                    created = sql_get_group_info(0, "created", gid=p[1])

                    dtime = datetime.utcnow()
                    payment_del(p[0], p[2], int(am[:-3]), p[1], time=dtime.strftime("%d.%m.%Y %H:%M"))
                    group_name = sql_get_group_name(p[1])
                    bot.send_message(chat_id=p[2],
                                     text=f"Ура, вижу твою оплату!\n\nГруппа {group_name}\n\nСейчас обновлю тариф.")
                    bot.send_animation(chat_id=p[2],
                                       animation="https://media.giphy.com/media/V1dH38rUl9yX7xU8nh/giphy.gif")

                    if p[3] in ('plan2', 'plan4', 'plan12', 'plan13'):
                        emo = "🤡🤡🤡"
                    elif p[3] in ('plan231'):
                        emo = "💰💰💰"
                    elif p[3] == 'plan232':
                        emo = "💵💵💵"
                    elif p[3] == 'plan233':
                        emo = "👑👑👑"
                    src = sql_get_user_info(p[2], 'src')
                    if src:
                        src_txt = f"\n🍓 {src}"
                    else:
                        src_txt = ''
                    bot.send_message(chat_id=admin_list[0], text=f"{emo} {am}\nГруппа: {group_name} {p[1]}\n"
                                                                 f"Юзер: {get_user_name(p[2])} {p[2]}\nСоздана {created}{src_txt}")
                    logger.info(f"Оплата {p[3]}. Группа {group_name} {p[1]}. "
                                f"Юзер {get_user_name(p[2])} {p[2]}")
                else:
                    conn = sqlite3.connect("mydb.ext")
                    cursor = conn.cursor()
                    cursor.execute("""SELECT utchour FROM payments where payment_id = (?)""", (p[0],))
                    paym_hour = cursor.fetchall()[0][0]
                    paym_timedelta = int(datetime.utcnow().hour) - paym_hour
                    # print('*** timedelta', paym_timedelta)
                    if (paym_timedelta >= 2) or (paym_timedelta < 0):
                        print('*** Платеж был старый:', datetime.utcnow().hour, paym_hour, paym_timedelta)
                        payment_del(p[0], success=0)
            except domain.exceptions.not_found_error.NotFoundError:
                # print('* Левый платёж')
                pass
            except domain.exceptions.api_error.ApiError:
                print('* Ошибка сервера API')
    except:
        pass
    t0 = PokaTimer(10, do_tick, bot, 'nothing')
    t0.start()
    # print('tick')




def check_plan(bot, update, args):
    uid = update.message.chat_id
    if len(args):
        gid = args[0]
    else: return 0
    act_plan = sql_get_group_info(0, 4, gid=gid)
    bot.send_message(chat_id=uid, text=act_plan)
    # bot.send_message(chat_id=admin_list[0], text="💵💵💵 %s" % p[2])
    # update_plan(p[1], p[3])

def set_plan(bot, update, args):
    uid = update.message.chat_id
    if str(uid) in admin_list:
        if len(args) == 2:
            gid = args[0]
        else:
            return
        act_plan = sql_get_group_info(0, 'plan', gid=gid)
        update_plan(gid, args[1])
        adminid = sql_get_group_info(0, 'admin', gid)
        bot.send_message(chat_id=adminid, text="Тариф сейчас обновится!")
        bot.send_animation(chat_id=adminid, animation="https://media.giphy.com/media/V1dH38rUl9yX7xU8nh/giphy.gif")
        bot.send_message(chat_id=uid, text='Изменил тариф группе %s c %s на %s' % (gid, act_plan, args[1]))
    else:
        print('Access Denied 560')


def check_conv(bot, update, args):
    # проверка конверсии
    with open('source.txt', 'r') as file:
        uids = file.readlines()
        print('*** UIDS:', uids)
        res = ''
        for u in uids:
            uu = u[3:-1]
            src = u[0:2]
            conn = sqlite3.connect("mydb.ext")
            cursor = conn.cursor()
            cursor.execute("""SELECT groupid FROM users WHERE userid = (?)""", (uu,))
            fe = cursor.fetchall()
            print('*** нашел группы:', fe)
            for f in fe:
                gid = f[0]
                plan = sql_get_group_info(0, 4, gid=gid)
                res += '\n%s=%s=%s' % (src, uu, plan)

        bot.send_message(chat_id=admin_list[0], text=res)

