# coding=utf-8
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from threading import * #Timer, Thread
import telegram
import sqlite3
import logging
import datetime
from time import sleep
import random
import copy
import httplib2

from sql import *
from classes import *
from settings import *
from datetime import datetime, timedelta


def insert_to_ids(uid, gid, tg_username, src):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT userid FROM ids WHERE userid = ?"
    cursor.execute(sql, (uid,))
    b = cursor.fetchall()
    if len(b) == 0:
        dtime = datetime.utcnow()
        cursor.execute(
            """INSERT INTO ids (userid, activegroup, registration_date, tg_username, src) VALUES (?,?,?,?,?)""",
            (uid, gid, dtime.strftime("%d.%m.%Y %H:%M"), tg_username, src))
        conn.commit()


def addusertodb(uid, gid, tg_username=0, check=0, src=None):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()

    if sql_get_group_info(0, 'open', gid) == "0":  # если группа закрылась
        return -1

    sql = "SELECT * FROM users WHERE userid = ? and groupid = ?"
    cursor.execute(sql, (uid, gid))
    a = cursor.fetchall()
    if len(a) == 0:
        if check:
            return 1
        act_plan = sql_get_group_info(0, 'plan', gid=gid)
        # print('Новенький, добавляю', id, act_plan, 'даю', plans[act_plan]['max_messages'])
        cursor.execute("""INSERT INTO users (userid, groupid, name, state, mes_left) VALUES (?,?,?,?,?)""",
                       (uid, gid, "new", "1", plans[act_plan]['max_messages']))
        conn.commit()
        sql = "SELECT userid FROM ids WHERE userid = ?"
        cursor.execute(sql, (uid,))
        b = cursor.fetchall()
        if len(b) == 0:
            insert_to_ids(uid, gid, tg_username, src)
            return 2
        else:
            cursor.execute("""UPDATE ids SET activegroup = ? WHERE userid = ?""", (gid, uid))
            conn.commit()
            return 1
    else:
        # print('Есть такой юзер в этой группе', a[0][0])
        return 0


def set_group_name(uid, name):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    while 1:
        newgid = random.randint(100000, 999999)
        sql = "SELECT groupid FROM groups WHERE groupid = ?"
        cursor.execute(sql, (newgid,))
        a = cursor.fetchall()
        if len(a) == 0:
            break
    dtime = datetime.utcnow()
    cursor.execute("""INSERT INTO groups (groupid, name, open, sorted, adminid, plan, created, sorting) VALUES (?,?,?,?,?,?,?,?)""",
                   (newgid, name, "1", "0", uid, 'plan1', str(dtime.strftime("%d.%m.%Y %H:%M")), "0"))
    conn.commit()
    print("Новая группа, ID %s, имя %s" % (newgid, name))
    cursor.execute("""INSERT INTO users (groupid, userid, name, state, mes_left) VALUES (?,?,?,?,?)""",
                   (newgid, uid, "new", "10", plans['plan1']['max_messages']))
    conn.commit()
    sql = "SELECT userid FROM ids WHERE userid = ?"
    cursor.execute(sql, (uid,))
    b = cursor.fetchall()
    if len(b) == 0:
        # print('*** regdate:', dtime.strftime("%d.%m.%Y %H:%M"))
        cursor.execute("""INSERT INTO ids (userid, activegroup, registration_date) VALUES (?,?,?)""",
                       (uid, newgid, str(dtime.strftime("%d.%m.%Y %H:%M"))))
    else:
        cursor.execute("""UPDATE ids SET activegroup = ? WHERE userid = ?""", (newgid, uid))
    conn.commit()

def set_user_name(id, name):
    ag = get_user_active_group(id)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    print('Новое имя', name, 'для', id, 'в группе', sql_get_group_name(ag))
    cursor.execute("""UPDATE users SET name  = (?) WHERE userid = (?) and groupid = (?)""", (name, id, ag))
    conn.commit()

def check_uniq_name(id, name):
    ag = get_user_active_group(id)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT name FROM users WHERE name = ? and userid = ? and groupid = ?"
    # print('Проверка уникальности', name, id, ag)
    cursor.execute(sql, (name, id, ag))
    a = cursor.fetchall()
    if len(a):
        # print('Повтор имени')
        return 0
    else:
        # print('Имя уникально')
        return 1

def get_user_state(id):
    ag = get_user_active_group(id)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT state FROM users WHERE userid = ? and groupid = ?"
    cursor.execute(sql, (id, ag))
    a = cursor.fetchall()
    if len(a):
        #print('Юзерстейт', a[0][0])
        return a[0][0]
    else:
        pass
        #print('Не нашел такого юзера', id, ag)


def set_user_state(id, st, gid=0):
    if gid == 0:
        ag = get_user_active_group(id)
    else:
        ag = gid
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    #print('Новый стейт', st, id, ag)
    cursor.execute("""UPDATE users SET state = (?) WHERE userid = (?) and groupid = (?)""", (st, id, ag))
    conn.commit()


def get_user_name(uid, gid=0):
    ag = get_user_active_group(uid)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT name FROM users WHERE userid = ? and groupid = ?"
    if gid == 0:
        cursor.execute(sql, (uid, ag))
    else:
        cursor.execute(sql, (uid, gid))
    a = cursor.fetchall()
    if len(a):
        #print('Юзернейм', a[0][0])
        return a[0][0]
    else:
        pass
        #print('Не нашел такого юзера', id, ag)


def get_goodboy(uid, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT goodboy FROM users WHERE userid = ? and groupid = ?"
    cursor.execute(sql, (uid, gid))
    a = cursor.fetchall()
    if len(a):
        return a[0][0]
    else:
        pass
        #print('Нет гудбоя', uid, gid)


def get_all_goodboys(gid, uids=0):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "select u.name, u2.name from users u join users u2 ON u.goodboy = u2.userid where u.groupid = (?) AND u2.groupid = (?)"
    cursor.execute(sql, (gid, gid))
    a = cursor.fetchall()
    if len(a):
        t = 'Вот секретный список!\nТолько тс-с-с 🤫\nСлева Санта, справа подопечный\n'
        for i in a:
            t += str('\n🎅🏾 %s ➡ %s' % (i[0], i[1]))
        return t
    else:
        return None
        #print('Нет гудбоя', uid, gid)



def set_user_active_group(uid, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE ids SET activegroup = (?) WHERE userid = (?)""", (gid, uid))
    conn.commit()


def get_user_active_group(id):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT activegroup FROM ids WHERE userid = ?"
    cursor.execute(sql, (id,))
    a = cursor.fetchall()
    if len(a):
        #print('Активная группа', a[0][0])
        return a[0][0]
    else:
        #print('Не нашел такого юзера', id)
        return 0

def search_key(key):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT groupid FROM groups WHERE groupid = ?"
    cursor.execute(sql, (key,))
    a = cursor.fetchall()
    if len(a):
        #print('Нашел группу', a[0][0])
        return a[0][0]
    else:
        #print('Не нашел такой группы', key)
        return 0


def sql_get_group_name(gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT name FROM groups WHERE groupid = ?"
    cursor.execute(sql, (gid,))
    a = cursor.fetchall()
    if len(a):
        #print('Имя группы', a[0][0])
        return a[0][0]
    else:
        return None
        #print('Не нашел такой группы', a[0][0])


def sql_get_group_info(uid, lalala, gid=0):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if lalala in (0, 'info'):
        sql = "SELECT info FROM groups WHERE groupid = ?"
    if lalala in (1, 'time'):
        sql = "SELECT time FROM groups WHERE groupid = ?"
    if lalala in (2, 'open'):
        sql = "SELECT open FROM groups WHERE groupid = ?"
    if lalala == 'sorted':
        sql = "SELECT sorted FROM groups WHERE groupid = ?"
    if lalala == 'sorting':
        sql = "SELECT sorting FROM groups WHERE groupid = ?"
    if lalala in (4, 'plan'):
        sql = "SELECT plan FROM groups WHERE groupid = ?"
    if lalala == 'admin':
        sql = "SELECT adminid FROM groups WHERE groupid = ?"
    if lalala == 'created':
        sql = "SELECT created FROM groups WHERE groupid = ?"
    if lalala == 'paid':
        sql = "SELECT paid FROM groups WHERE groupid = ?"
    if gid == 0:
        ag = get_user_active_group(uid)
        cursor.execute(sql, (ag,))
    else:
        cursor.execute(sql, (gid,))
    a = cursor.fetchall()
    if len(a):
        # print('Инфо группы', a[0][0])
        if a[0][0] is None:
            return "-"
        else:
            return a[0][0]
    else:
        return '---'


def get_user_info(uid, lalala, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if lalala == 0:
        sql = "SELECT info0 FROM users WHERE userid = ? and groupid = ?"
    if lalala == 1:
        sql = "SELECT info1 FROM users WHERE userid = ? and groupid = ?"
    cursor.execute(sql, (uid, gid))
    a = cursor.fetchall()
    if len(a):
        #print('Инфо человека:', a[0][0])
        if a[0][0] is None:
            return "-"
        else:
            return a[0][0]


def set_user_info(id, lalala, info):
    ag = get_user_active_group(id)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if lalala == 0:
        cursor.execute("""UPDATE users SET info0 = (?) WHERE userid = (?) and groupid = (?)""", (info, id, ag))
    if lalala == 1:
        cursor.execute("""UPDATE users SET info1 = (?) WHERE userid = (?) and groupid = (?)""", (info, id, ag))
    conn.commit()

def set_group_info(uid, lalala, info, gid=0):
    if gid == 0:
        gid = get_user_active_group(uid)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if lalala == 0:
        cursor.execute("""UPDATE groups SET info = (?) WHERE groupid = (?)""", (info, gid))
    if lalala == 1:
        cursor.execute("""UPDATE groups SET time = (?) WHERE groupid = (?)""", (info, gid))
    if lalala == 2:
        cursor.execute("""UPDATE groups SET key = (?) WHERE groupid = (?)""", (info, gid))
    if lalala == 3:
        cursor.execute("""UPDATE groups SET name = (?) WHERE groupid = (?)""", (info, gid))

    conn.commit()


def update_plan(gid, plan):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE groups SET plan = (?) WHERE groupid = (?)""", (plan, gid))
    conn.commit()
    dtime = datetime.utcnow()
    cursor.execute("""UPDATE groups SET paid = (?) WHERE groupid = (?)""", (dtime.strftime("%d.%m.%Y %H:%M"), gid))
    conn.commit()
    cursor.execute("""UPDATE users SET mes_left = (?) WHERE groupid = (?)""", (plans[plan]['max_messages'], gid))
    conn.commit()



def set_goodboy(uid, boyid, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE users SET goodboy = (?) WHERE userid = (?) and groupid = (?)""", (boyid, uid, gid))
    conn.commit()


def set_group_sorted(gid, s):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE groups SET sorted = (?) WHERE groupid = (?)""", (s, gid))
    conn.commit()

def set_group_sorting(gid, s):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE groups SET sorting = (?) WHERE groupid = (?)""", (s, gid))
    conn.commit()
    logger.info(f"Флаг Sorting группы {gid} = {s}")



def check_group_sorted(gid, checksorting=0):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT ? FROM groups WHERE groupid = ?"
    if checksorting:
        srt = 'sorting' #
    else:
        srt = 'sorted'
    cursor.execute(sql, (srt, gid))
    a = cursor.fetchall()
    if len(a):
        if a[0][0] == "0":
            return 0
        if a[0][0] == "1":
            return 1
    else:
        #print("Не нашел такой группы")
        return -1


def get_all_groups(id):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT groupid FROM users WHERE userid = ?"
    cursor.execute(sql, (id,))
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    else:
        pass
        #print("Не нашел ни одной группы")
    return b

def get_group_users(gid, ineedid=False):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if ineedid:
        sql = "SELECT userid FROM users WHERE groupid = ?"
    else:
        sql = "SELECT name FROM users WHERE groupid = ?"
    cursor.execute(sql, (gid,))
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        # print("Показываем участников группы", get_group_name(gid))
        for i in a:
            b.append(i[0])
            #print(i[0])
    else:
        pass
    return b

def get_group_users2(gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT name, userid FROM users WHERE groupid = ?"
    cursor.execute(sql, (gid,))
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            # b.append({'name': i[0], 'uid': i[1], 'tg_username': sql_get_tg_username(i[1])})
            b.append({'name': i[0], 'uid': i[1], 'tg_username': sql_get_user_info(i[1], "tg_username")})
    return b


def sql_get_user_info(uid, type):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if type == 'tg_username':
        sql = "SELECT tg_username FROM ids WHERE userid = ?"
    elif type == 'src':
        sql = "SELECT src FROM ids WHERE userid = ?"
    cursor.execute(sql, (uid,))
    a = cursor.fetchall()
    try:
        return a[0][0]
    except:
        return 0


def check_admin(id):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT groupid FROM groups WHERE adminid = ?"
    cursor.execute(sql, (id,))
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    else:
        #print("Нигде не админ")
        pass
    #print(b)
    return b

def amiingroup(uid, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT userid FROM users WHERE userid = ? and groupid = ?"
    cursor.execute(sql, (uid, gid))
    a = cursor.fetchall()
    if len(a) > 0:
            #print("Юзер в группе")
            return 1
    else:
        #print("Юзер не в группе")
        return 0

def open_close_group(gid, op):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if op == "0":
        cursor.execute("""UPDATE groups SET open = (?) WHERE groupid = (?)""", ("0", gid))
    if op == "1":
        cursor.execute("""UPDATE groups SET open = (?) WHERE groupid = (?)""", ("1", gid))
    conn.commit()


def del_santas(gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE users SET goodboy = (?) WHERE groupid = (?)""", ("", gid))
    conn.commit()
    print("Отменено распределение Сант в группе", sql_get_group_name(gid))


def del_group(gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""DELETE FROM groups WHERE groupid = (?)""", (gid, ))
    conn.commit()
    cursor.execute("""DELETE FROM users WHERE groupid = (?)""", (gid,))
    conn.commit()
    cursor.execute("""UPDATE ids SET activegroup = (?) WHERE activegroup = (?)""", ("0", gid))
    conn.commit()
    print("Группа удалена %s" % gid)


def del_user_from_group(uid, gid):
    print(f'uid {uid} gid {gid}')
    print("Юзер %s удалился из группы %s" % (get_user_name(uid), gid))
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""DELETE FROM users WHERE userid = (?) and groupid = (?)""", (uid, gid))
    conn.commit()
    cursor.execute("""UPDATE ids SET activegroup = (?) WHERE userid = (?)""", ("0", uid))
    conn.commit()


def get_admin_users():
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT adminid FROM groups"
    cursor.execute(sql)
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    return b


def get_all_users():
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT userid FROM ids"
    cursor.execute(sql)
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    return b


def get_some_users():  #2 января
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT DISTINCT users.userid FROM users join groups ON users.groupid == groups.groupid WHERE groups.sorted = (?)"
    cursor.execute(sql, ("1",))
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    return b



def addpayment(payment_id, gid, uid, plan, utchour):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""INSERT INTO payments (payment_id, groupid, userid, plan, utchour) VALUES (?,?,?,?,?)""",
                   (payment_id, gid, uid, plan, utchour))
    conn.commit()

def get_all_payments():
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT * FROM payments"
    cursor.execute(sql)
    a = cursor.fetchall()
    return a


def payment_del(pid, uid=1, amount=1, gid=0, success=1, time=0):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""DELETE FROM payments WHERE payment_id = (?)""", (pid,))
    print('*** Удалил платёж', pid)
    conn.commit()
    if success:
        # вставляем незаполненный чек
        cursor.execute("""INSERT INTO checks (uid, gid, amount, time) VALUES (?, ?, ?, ?)""", (uid, gid, amount, time))
        print('***** Вставил в чеки uid, gid, am, time', uid, gid, amount, time)
        conn.commit()


def addcheck(uid, amount, checkid):
    # ищем первый подходящий чек с нужной суммой
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""SELECT gid FROM checks where uid = (?) and amount = (?) and checkid is Null""", (uid, amount))
    a = cursor.fetchall()
    if len(a):
        gid = a[0][0]
        cursor.execute("""UPDATE checks SET checkid = (?) WHERE gid = (?) and amount = (?)""", (checkid, gid, amount))
        conn.commit()
        return 1
    else:
        return 0


def ch2_sql(uid, gid, checkid, amount):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""INSERT INTO checks (uid, gid, checkid, amount) VALUES (?, ?, ?, ?)""", (uid, gid, checkid, amount))
    conn.commit()


def get_all_groups_with_some_plan(p):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""SELECT groupid FROM groups where plan = (?)""", (p,))

    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    else:
        pass
        #print("Не нашел ни одной группы")
    return b

def mes_act(bot, update):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()

    cursor.execute("""SELECT userid, groupid FROM users where mes_left is Null""")
    a = cursor.fetchall()
    if len(a) > 0:
        for i in a:
            uid = i[0]
            gid = i[1]
            act_plan = sql_get_group_info(0, 4, gid=gid)
            cursor.execute("""UPDATE users SET mes_left = (?) WHERE userid = (?) and groupid = (?)""",
                           (plans[act_plan]['max_messages'], uid, gid))
            conn.commit()



def send_mes_check(uid, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""SELECT mes_left FROM users where userid = (?) and groupid = (?)""", (uid, gid))
    a = cursor.fetchall()
    if len(a) > 0:
        if a[0][0] > 0:
            cursor.execute("""UPDATE users SET mes_left = (?) WHERE userid = (?) and groupid = (?)""", (a[0][0] - 1, uid, gid))
            conn.commit()
            return a[0][0]
        else:
            print('СЧЕТЧИК 0')
            return 0
    else:
        print('*** ERROR Ошибка в проверке счетчика сообщений', uid, gid)
        return 1


def sql_ab(uid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""SELECT ab FROM ab1 where uid = (?)""", (uid,))
    a = cursor.fetchall()
    if len(a) > 0:
        return a[0][0]
    else:
        ab = random.choice(('A', 'B'))
        cursor.execute("""INSERT INTO ab1 (uid, ab) VALUES (?, ?)""", (uid, ab))
        conn.commit()
        return ab

def get_pay_date(gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""SELECT time FROM checks where gid = (?)""", (gid,))
    a = cursor.fetchall()
    res = 'free'
    if len(a) > 0:
        res = a[0][0]
    return res

def sql_find_group_by_name(name):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""SELECT groupid, created, adminid FROM groups where name = (?)""", (name,))
    a = cursor.fetchall()
    return a




