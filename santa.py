# coding=utf-8
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from threading import Thread
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, LabeledPrice, ShippingOption
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters, PreCheckoutQueryHandler, ShippingQueryHandler
import telegram
import sqlite3
import logging
import datetime
from time import sleep
import copy
import httplib2
# from oauth2client.service_account import ServiceAccountCredentials
import random
from sql import *
from classes import *
from settings import *
from pay import *
import uuid
from yookassa import Configuration, Payment

def stata(bot, update, args):
    class ArrThread(Thread):
        def __init__(self, name):
            Thread.__init__(self)
            self.name = name

        def run(self):
            print('STAT2')
            uid = update.message.chat_id
            conn = sqlite3.connect("mydb.ext")
            cursor = conn.cursor()
            sql = "SELECT groupid FROM groups"
            cursor.execute(sql)
            a = cursor.fetchall()
            gist = {}
            keys = range(300)
            for k in keys:
                gist[k] = 0
            print('INITIAL GIST', gist)
            for a1 in a:
                ulist = get_group_users(a1[0])
                l = len(ulist)
                try:
                    gist[l] += 1
                except:
                    pass
                print(l)
            print(gist)
            t = ''
            for k in keys:
                t += f"{gist[k]}\n"
            bot.send_message(chat_id=uid, text=t)

    my_thread = ArrThread("StatThread")
    my_thread.start()


def ads(bot, update, args):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    conn.set_trace_callback(print)
    sql = "SELECT userid, src FROM ids where src is not Null"
    cursor.execute(sql)
    a = cursor.fetchall()
    print(a)
    text = 'ads'
    for a1 in a:
        sql = "SELECT groupid, plan from groups where adminid = (?)"
        cursor.execute(sql, (a1[0],))
        b = cursor.fetchall()
        if len(b):
            print(b)
            for b1 in b:
                if b1[1] in ('plan12', 'plan13'):
                    text += f"\n{b1}"
        else:
            print("IM HERE")
            sql = "SELECT groupid from users where userid = ?"
            cursor.execute(sql, (a1[0]), )
            c = cursor.fetchall()
            print(f"CCC {c}")
            if len(c):
                print(c)
                for c1 in c:
                    sql = "SELECT plan from groups where groupid = ?"
                    cursor.execute(sql, (c1[0]), )
                    d = cursor.fetchall()
                    if len(d):
                        print(d)
                        for d1 in d:
                            text += f"\n{d1}"
    bot.send_message(chat_id="182116723", text=text)


def arrange(bot, gid, uid, mid, query):
    class ArrThread(Thread):
        def __init__(self, name):
            Thread.__init__(self)
            self.name = name

        def run(self):
            print(f"Запущен тред распределения {gid}")
            logger.info(f"Запущен тред распределения {gid}")
            #if check_group_sorted(gid, checksorting=1):
            if str(sql_get_group_info(0, 'sorting', gid)) == "1":
                print("*** АХТУНГ, ГРУППА УЖЕ СОРТИРУЕТСЯ")
                logger.info(f"АХТУНГ, ГРУППА УЖЕ СОРТИРУЕТСЯ {gid}")
                query.answer(text='Распределение уже запущено, идёт жеребьевка и рассылка сообщений')
                return
            else:
                set_group_sorting(gid, "1")
            empty_markup = telegram.InlineKeyboardMarkup([[]])
            keyboard = [[]]
            text = "❄️❄️❄️❄️❄️\nДа случится волшебство!"
            santalist = get_group_users(gid, ineedid=True)
            blocked_list = []
            mes_list = {}
            # делаем рассылку простых сообщений
            # если сработал экцепшн, добавляем в массив заблокированных
            # в конце возвращаем -2 и массив
            # TODO заменить на рассылку статусов
            for u in santalist:
                try:
                    mes_test = bot.send_message(chat_id=u, text='Начинаем распределение...')
                    mes_list[u] = mes_test['message_id']
                except telegram.error.Unauthorized:
                    blocked_list.append(u)
            if len(blocked_list):
                keyboard = [[InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)]]
                text = 'Проблема!\nУ некоторых участников твоей группы я заблокирован, или он удалил чат со мной.\n\n' \
                       'Попроси их разблокировать меня или удали этих людей из списка.\n\n'
                for i in blocked_list:
                    text += get_user_name(i, gid)
                    tg_nick = sql_get_user_info(i, 'tg_username')
                    if tg_nick: text += f" {tg_nick}"
                    text += '\n'
                markup = telegram.InlineKeyboardMarkup(keyboard)
                bot.edit_message_text(text=text, chat_id=uid, message_id=mid, reply_markup=markup)
                print('Не могу распределить группу %s. Бот заблокирован у %s.' % (gid, blocked_list))
                logger.info(f"Не могу распределить группу {gid}. Бот заблокирован у {blocked_list}.")
                for u in mes_list.keys():
                    bot.edit_message_text(text='Не получилось распределить', chat_id=u, message_id=mes_list[u])
                set_group_sorting(gid, "0")
                return

            santas_goodboys = dict.fromkeys(santalist, 0)
            nosort = 1
            deadend = 0

            while nosort:
                print("Запускаю сортировку для группы", gid)
                logger.info(f"Запускаю сортировку для группы {gid}")
                deadend = 0
                nogiftlist = santalist[:]

                for santa in santalist:
                    while 1:
                        goodboy = nogiftlist[random.randint(0, len(nogiftlist)-1)]
                        if goodboy == santa:
                            if len(nogiftlist) == 1:
                                print("Тупик, необходимо начать распределение сначала")
                                logger.info(f"Тупик, необходимо начать распределение сначала")
                                deadend = 1
                                break
                            else:
                                print("Нельзя дарить самому себе")
                                logger.info(f"Нельзя дарить самому себе")
                        else:
                            break
                    if deadend:
                        break
                    nogiftlist.remove(goodboy)
                    santas_goodboys[santa] = goodboy # вместо set_goodboy(santa, goodboy, gid)
                if deadend:
                    continue

                # финальная проверка
                if 0 in santas_goodboys.values():
                    print("Ошибка, кому-то не досталось")
                    logger.info(f"Ошибка, кому-то не досталось")
                    continue
                unique_values = [k for k, v in santas_goodboys.items() if
                                 list(santas_goodboys.values()).count(v) == 1]
                if len(unique_values) < len(santas_goodboys):
                    print("Задвоение подопечных")
                    logger.info(f"Задвоение подопечных")
                    continue
                ttt = ''
                for k, v in santas_goodboys.items():
                    set_goodboy(k, v, gid)
                    sleep(0.1)
                    print(k, v)
                    logger.info(f"{k} {v}")
                    ttt += f'{k} {v}\n'
                # bot.send_message(chat_id="182116723", text=f"Sort группа {sql_get_group_name(gid)}, len {len(santalist)}"
                #                                            f"\n\n{ttt}")

                count_test = 0
                for santa in santalist:
                    goodboy = get_goodboy(santa, gid)
                    sleep(0.1)
                    text1 = f"Хоу-хоу-хоу, <b>{get_user_name(santa, gid)}</b>!\n\n✨ Волшебство произошло " \
                            f"в группе <b>{sql_get_group_name(gid)}</b>.\n" \
                            f"Мои олени выбрали для тебя классного подопечного.\n\n" \
                            f"🥷 Это <b>{get_user_name(goodboy, gid)}</b>\nПравда здорово?\n\n"
                    text2 = f"😙 Однажды этот человек, сидя на моих коленях, шепнул мне на ушко, что хочет (но выбор, " \
                            f"конечно, за тобой):\n<i>{get_user_info(goodboy, 0, gid)}</i>\n\n"
                    text3 = f"🗺 Кстати, вот где он будет во время вручения подарочков:\n" \
                            f"<i>{get_user_info(goodboy, 1, gid)}</i>\n\n"\
                            f"🕊 Для отправки ему анонимного сообщения напиши:\n/sendgoodboy {gid} текст сообщения\n" \
                            f"Для связи со своим Сантой:\n/sendsanta {gid} сообщение\n\n" \
                            f"Классный бот? Если ты так тоже считаешь, жми /thanks"
                    print("Отправляю подопечного Санте", santa, get_user_name(santa, gid=gid), goodboy, get_user_name(goodboy, gid=gid))
                    logger.info(f"Отправляю подопечного Санте, {santa}, {get_user_name(santa, gid=gid)}, {goodboy}, {get_user_name(goodboy, gid=gid)}")
                    # if any(s in get_user_info(goodboy, 0, gid) for s in bad_symbols) or \
                    #         any(s in get_user_info(goodboy, 1, gid) for s in bad_symbols):
                    #     bot.edit_message_text(message_id=mes_list[santa], chat_id=santa, text=text)
                    #     print("Плохие символы в сообщении Санте, отправляю без маркдауна")
                    try:
                        bot.edit_message_text(message_id=mes_list[santa], chat_id=santa, text=text1, parse_mode=telegram.ParseMode.HTML)
                    except:
                        bot.edit_message_text(message_id=mes_list[santa], chat_id=santa, text=text1)
                    bot.send_chat_action(chat_id=santa, action=telegram.ChatAction.TYPING)
                    sleep(0.1)
                    try:
                        bot.send_message(chat_id=santa, text=text2, parse_mode=telegram.ParseMode.HTML)
                    except:
                        bot.send_message(chat_id=santa, text=text2)
                    bot.send_chat_action(chat_id=santa, action=telegram.ChatAction.TYPING)
                    sleep(0.1)
                    try:
                        bot.send_message(chat_id=santa, text=text3, parse_mode=telegram.ParseMode.HTML)
                    except:
                        bot.send_message(chat_id=santa, text=text3)
                    print("Отправил")
                    logger.info(f"Отправил")
                    sleep(0.1)
                    gifs = ['https://media.giphy.com/media/2T0KlJD1Hm3sc/giphy.gif',
                            'https://media.giphy.com/media/UO95NWY0PmoWk/giphy.gif',
                            'https://media.giphy.com/media/3o7TKNoUpiqQX6ot1e/giphy.gif',
                            'https://media.giphy.com/media/3mJq8i2W3qbe9AkxgL/giphy.gif',
                            'https://media.giphy.com/media/3o6Mb6TaW5qzmTmGaI/giphy.gif',
                            'https://media.giphy.com/media/hXkCyeZmQ46tEK163Z/giphy.gif']
                    link = gifs[random.randint(0, len(gifs)-1)]
                    bot.send_animation(chat_id=santa, animation=link)
                    count_test += 1

                nosort = 0
                set_group_sorted(gid, "1")
                set_group_sorting(gid, "0")
                open_close_group(gid, "0")
            print("Закончил сортировку для группы", gid)
            paid_date = get_pay_date(gid)
            str_paid_date = '' if paid_date == 'free' else f", оплачена {paid_date}"
            logger.info(f"Закончил сортировку для группы {gid} {sql_get_group_name(gid)} {len(santalist)} юзеров "
                        f"{str_paid_date}")
            bot.edit_message_text(chat_id=uid, message_id=mid, text='☃️☃️☃️', reply_markup=empty_markup)
            bot.send_message(chat_id="182116723", text=f"☑️ Отсортирована группа {sql_get_group_name(gid)}, "
                                                       f"{len(santalist)} участников {str_paid_date}")

    my_thread = ArrThread("ArrangeThread")
    my_thread.start()


def convert_link(link):
    return link
    if 'ozon.ru' not in link:
        return link
    words_new = ''
    words = link.split(' ')
    # print(words)
    for w in words:
        # print(w)
        if 'ozon.ru' in w:
            if '?' in w:
                words_new += '%s&code=OZON0I6IL' % w
            else:
                if w[-1] == '/':
                    words_new += '%s?&code=OZON0I6IL' % w
                else:
                    words_new += '%s/?code=OZON0I6IL' % w
        else:
            words_new += w
        words_new += ' '
    return words_new


def gogogo(bot, update):
    pass

def gogogo2(bot, update):
    class MyThread(Thread):
        def __init__(self, name):
            Thread.__init__(self)
            self.name = name

        def run(self):
            amount = random.randint(3, 15)
            msg = "%s is running" % self.name
            # print(msg)

            ul = get_all_users()
            # ul = ['182116723', '165799973']
            bot.send_message(chat_id="182116723", text="Начинаю рассылку, целей: %s" % len(ul))
            i = 0
            y = 0
            for u in ul:
                text = "Предновогоднее обновление!\nНовый функционал позволяет анонимно общаться Сантам и их подопечным.\n\n" \
                       "Пример команд:\n" \
                       "/sendgoodboy 12345 Привет, напиши свой почтовый индекс\n" \
                       "/sendsanta 12345 Санта, не отправляй мне угли!\n" \
                       "Вместо 12345 нужно подставить ID для каждой группы. Узнать ваш ID можно в описании группы, в самом низу."
                sleep(0.1)
                i += 1
                try:
                    bot.send_message(chat_id=u, text=text)
                    conn = sqlite3.connect("mydb.ext")
                    cursor = conn.cursor()
                    cursor.execute("""INSERT INTO alive_users (uid) VALUES (?)""", (u,))
                    conn.commit()
                except telegram.error.Unauthorized:
                    y += 1
            print("Сделал рассылку по %s юзерам\nЗаблокировано: %s" % (i, y))
            bot.send_message(chat_id="182116723", text="Сделал рассылку по %s юзерам\nЗаблокировано: %s" % (i, y))
    my_thread = MyThread("SpamThread")
    my_thread.start()


def trista(bot, update, args):
    print("/300", update.message.chat_id)
    all_list = get_all_users()
    bot.send_message(chat_id=update.message.chat_id, text=len(all_list))

    import pandas as pd
    conn = sqlite3.connect("mydb.ext", isolation_level=None, detect_types=sqlite3.PARSE_COLNAMES)
    db_df = pd.read_sql_query("SELECT userid FROM ids", conn)
    db_df.to_csv('users.csv', index=False)
    bot.send_document(update.message.chat_id, open('users.csv', 'rb'))


def help_com(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text='https://teletype.in/@mind_digits/ViRyU2LS4Lr')
    logger.info(f'helped {update.message.chat_id}')


def spam_admin(bot, update, args):
    uid = update.message.from_user.id
    text = ''
    for i in args:
        text = text + i + " "
    text = text[0:-1]
    ulist = get_admin_users()
    for i in ulist:
        sleep(0.1)
        bot.send_message(chat_id=i, text=text, parse_mode=telegram.ParseMode.MARKDOWN)


def thanks(bot, update):
    uid = update.message.chat_id
    print("/thanks", uid)
    # custom_keyboard = [[, , ], ['Qiwi/Paypal']]

    pay_keyboard = [[]]
    pay_keyboard[0].append(InlineKeyboardButton('150❤️', callback_data='pay1:%s' % uid))
    pay_keyboard[0].append(InlineKeyboardButton('270💚', callback_data='pay2:%s' % uid))
    pay_keyboard[0].append(InlineKeyboardButton('450💙', callback_data='pay3:%s' % uid))
    pay_keyboard.append([InlineKeyboardButton('Qiwi/Paypal', callback_data='qiwipaypal')])

    reply_markup = telegram.InlineKeyboardMarkup(pay_keyboard)

    text = "Тайный РобоСанта 🎅🏾 всегда рад устроить праздник для хороших мальчиков и девочек.\n" \
           "Если вы считаете, что Санта тоже был <b>хорошим мальчиком</b>, можете подкинуть ему пару угольков на нового " \
           "робо-оленя и гирлянду для серверной стойки.\n" \
           "Это можно сделать по одной из кнопок" \
           # "\n%s"\
           # "\nhttps://qiwi.com/p/79269166273"\
           # "\nhttps://paypal.me/minddigits" % link
    bot.send_message(chat_id=uid, text=text, reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML)

    gifs = ['https://media.giphy.com/media/I1nwVpCaB4k36/giphy.gif',
            'https://media.giphy.com/media/474MaOHaHXyla/giphy.gif',
            'https://media.giphy.com/media/IeEX4eHLTejf95laVO/giphy-downsized-large.gif',
            'https://media.giphy.com/media/bZn9fl2SvZWR8qVjpp/giphy.gif']
    link = gifs[random.randint(0, 3)]
    bot.send_animation(chat_id=update.message.chat_id, animation=link)


def admin(bot, update):
    uid = update.message.from_user.id
    adm_grps(uid, bot, update=update)

def find_group_by_name(bot, update, args):
    uid = update.message.from_user.id
    if len(args):
        name = ''
        for a in args:
            name += f'{a} '
        name = name[:-1]
        t = sql_find_group_by_name(name)
        bot.send_message(chat_id=admin_list[0], text=t)

def show_group_to_me(bot, update, args):
    if len(args):
        gid = args[0]
        t = f'Подсмотрим группу {sql_get_group_name(gid)}'
        kb = [[InlineKeyboardButton('go', callback_data=f'showgroup:{gid}')]]
        rm = telegram.InlineKeyboardMarkup(kb)
        bot.send_message(chat_id=admin_list[0], text=t, reply_markup=rm)


def newgroup(bot, update, args):
    uid = update.message.from_user.id
    if len(args) > 0:
        name = ''
        for i in args:
            name = name + i + " "
        name = name[0:-1]
        if any(s in name for s in bad_symbols):
            bot.send_message(chat_id=update.message.chat_id, text="Нельзя использовать символы %s" % bad_symbols)
            return
        set_group_name(uid, name)
        text = "Новая группа <b>%s</b> создана. А теперь напиши мне короткое описание вашей группы. Может быть, " \
               "какие-то правила или рекомендации, ориентировочный бюджет. Или анекдот про оленей.\n" \
               "Место и время не пиши, для этого будет отдельный шаг." % name
    else:
        text = "Нужно указать имя будущей группы после команды.\nНапример:\n <code>/newgroup Озорные снежинки</code>"
    bot.send_message(chat_id=update.message.chat_id, text=text, parse_mode=telegram.ParseMode.HTML)


def start(bot, update, args):
    uid = update.message.from_user.id
    tg_username = update.message.from_user.username
    text = "Привет, я повелитель Тайных Сант. Как насчет немного подарочков?"
    t = ''

    if (len(args) > 0) and (len(str(args[0])) >= 6):
        print('/start ', args[0])
        gid = search_key(args[0])
        if gid:
            act_plan = sql_get_group_info(0, 'plan', gid=gid)
            ulist = get_group_users(gid)
            if len(ulist) < plans[act_plan]['max_users']:
                a = addusertodb(uid, gid, tg_username=tg_username)
                if len(get_group_users(gid)) == 2:
                    bot.send_message(chat_id="182116723", text="Группа *%s*" % sql_get_group_name(gid), parse_mode=telegram.ParseMode.MARKDOWN)
                if (a >= 1) or ((a == 0) and (get_user_state(uid) == "1")):
                    t = f"🎅 Приветствую хороших мальчиков и девочек, хоу-хоу-хоу! 🎅\n" \
                        f"Ты начал регистрироваться в операции <b>Тайный Санта</b> среди прекрасной команды " \
                        f"<b>{sql_get_group_name(gid)}</b>."
                    rm = telegram.InlineKeyboardMarkup(
                        [[InlineKeyboardButton("Ух ты! Дальше!", callback_data=f'reg_step2:{gid}')]])
                    bot.send_message(chat_id=uid, text=t, reply_markup=rm, parse_mode=telegram.ParseMode.HTML)
                    all_list = get_all_users()
                    if (len(all_list) % 1000 == 0) and (a == 3):
                        bot.send_message(chat_id="182116723", text="Ура, новые юзеры! Теперь их *%s*" % len(all_list))
                elif a == 0:
                    text = "Привет! Ты уже есть в этой группе, добро пожаловать назад. 🦌"
                elif a == -1:
                    text = "Регистрация в эту группу уже закрыта."
            else:
                upgrade_keyboard = [[InlineKeyboardButton("✨ Улучшить", callback_data='upgrade:%s:%s' % (uid, gid))]]
                rm = telegram.InlineKeyboardMarkup(upgrade_keyboard)
                t = 'Ой-ой! 🏴‍☠️🏴‍☠️🏴‍☠️\nЭта группа %s достигла своего предела!\n' \
                    'Максимум в неё может войти %s человек\n' \
                    'Что делать? Надо улучшить группу, это может сделать любой участник на странице группы.\n\n' \
                    'Когда будет выбран вариант улучшения, снова пройди по ссылке и нажми Старт.\n\n' \
                    'Возвращайся скорее!' % (sql_get_group_name(gid), plans[act_plan]['max_users'])
                bot.send_message(chat_id=uid, text=t, reply_markup=rm, parse_mode=telegram.ParseMode.HTML)
        else:
            text = 'Ключ не верифицирован'
    else:
        if (len(args) > 0) and ((len(str(args[0])) == 2) or (len(str(args[0])) == 3)):
            print('/start SOURCE ', args[0])
            with open('source.txt', 'a') as file:
                file.write(str(args[0] + ' %s\n' % uid))
            insert_to_ids(uid, 0, tg_username, args[0])
            bot.send_message(chat_id=admin_list[0], text=f'🍓 {args[0]} {uid}')
        text = text + '\nЧтоб присоединиться к группе, нужно пройти по секретной ссылке.\n' \
                      'Или создать свою группу командой /newgroup'
        text += '\n\nБот предназначен для уже существующих компаний, тут вы не ' \
                'найдёте группы, в которые можно вступить. Вы можете создать любое количество групп и пригласить в них ' \
                'своих друзей, родственников и знакомых. Можете запустить Тайного Санту у себя в школе/ в универе/ на работе. ' \
                'Возглавьте это новогоднее безумие и станьте душой своей компании!'
        text += '\n\nНовости наших ботов:\nt.me/aipunk_news'
    custom_keyboard = [['Мои группы'], ['Управление']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    if t:
        text = '🎅🏿'
    bot.send_message(chat_id=uid, text=text, parse_mode=telegram.ParseMode.HTML,
                     reply_markup=reply_markup)


def adm_grps(uid, bot, update=0, query=0):
    if update:
        uid = update.message.chat_id
    if query:
        uid = query.message.chat_id

    glist = check_admin(uid)
    groups_keyboard = [[]]
    reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
    j = 0
    if len(glist):
        text = "📟 Панель управления 📟\n\nТы 👑админ👑 в следующих группах:\n"
        for i in glist:
            text = text + "\n<b>" + sql_get_group_name(i) + "</b>"
            groups_keyboard.append([])
            groups_keyboard[j].append(
                InlineKeyboardButton("%s" % (sql_get_group_name(i)), callback_data='admgroup:%s' % i))
            j = j + 1
        text = text + "\n\n🧝‍♀️ Выбери группу и я покажу тебе кнопки всевластия."
    else:
        text = "Я ничего не нашел, кажется, ты не админ ни одной группы.\n\n" \
               "Чтоб создать свою, напиши:\n" \
               "<code>/newgroup Хлопушка в ушке</code>\n" \
               "(или придумай своё имя группы)"
    if update:
        try:
            bot.edit_message_reply_markup(uid, message_id=mes_with_kb[uid], reply_markup='')
        except:
            pass
        m = bot.send_message(chat_id=uid, text=text, reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML)
        mes_with_kb[uid] = m.message_id
    if query:
        bot.edit_message_text(text=text, chat_id=uid, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML)


def sh_grps(uid, bot, update=0, query=0):
    glist = get_all_groups(uid)
    text = 'Вот список твоих групп:\n\n'
    groups_keyboard = [[]]
    reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
    if len(glist) == 0:
        text = "Пока нет ни одной группы\n"
    else:
        for i in glist:
            text = text + "🟢 <b>" + sql_get_group_name(i) + "</b>\n"
            groups_keyboard.append(
                [InlineKeyboardButton("%s" % sql_get_group_name(i), callback_data='showgroup:%s' % i)])
    text = text + "\n\n👨‍👩‍👧‍👦 Для создания новой группы с блэкджеком и эльфами введи команду /newgroup\n" \
                  "\nУправлять своими созданными группами можно через кнопку Управление (<i>или команду /adm</i>)\n"
    if update:
        try:
            bot.edit_message_reply_markup(uid, message_id=mes_with_kb[uid], reply_markup='')
        except:
            pass
        m = bot.send_message(text=text, chat_id=update.message.chat_id, message_id=update.message.message_id,
                         reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML)
        mes_with_kb[uid] = m.message_id
    if query:
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                         reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML)


def senduser(bot, update, args):
    uid = update.message.from_user.id
    if len(args) < 2:
        bot.send_message(chat_id=uid, text='Неправильный формат')
        return
    user_message = ''
    for w in args[1:]:
        user_message += w
        user_message += ' '
    print('Goodboy mes:', user_message)
    users_uid = args[0]
    try:
        bot.send_message(chat_id=users_uid, text='‼️‼️‼️ <b>Сообщение от техподдержки:</b>\n%s' % (user_message), parse_mode=telegram.ParseMode.HTML)
        bot.send_message(chat_id=uid, text='Отправил')
    except telegram.error.Unauthorized:
        bot.send_message(chat_id=uid, text='Упс, ошибка. Твой получатель меня заблокировал :(')
            
            
def sendgoodboy(bot, update, args):
    uid = update.message.from_user.id
    if len(args) < 2:
        bot.send_message(chat_id=uid, text='Неправильный формат\nПример правильного:\n/sendgoodboy 12345 Пришли свой адрес')
        return
    gid = args[0]
    goodboy_message = ''
    for w in args[1:]:
        goodboy_message += w
        goodboy_message += ' '
    print('Goodboy mes:', goodboy_message)
    goodboy_message = convert_link(goodboy_message)
    gname = sql_get_group_name(gid)
    if gname is None:
        bot.send_message(chat_id=uid, text='Неправильный ID группы')
    else:
        goodboy = get_goodboy(uid, gid)
        try:
            bot.send_message(chat_id=goodboy, text='📬 Твой Санта из группы %s прислал сообщение:\n%s\nДля ответа введи команду\n'
                                               '📤\n/sendsanta %s твой ответ' % (gname, goodboy_message, gid), parse_mode=telegram.ParseMode.HTML)
            bot.send_message(chat_id=uid, text='Отправил')
        except telegram.error.Unauthorized:
            bot.send_message(chat_id=uid, text='Упс, ошибка. Твой получатель меня заблокировал :(')



def findsanta(uid, gid):
    ulist = get_group_users(gid, ineedid=True)
    santaid = None
    for u in ulist:
        goodboy = get_goodboy(u, gid)
        if str(goodboy) == str(uid):
            santaid = u
            return santaid
    return None


def sendsanta(bot, update, args):
    uid = update.message.from_user.id
    if len(args) < 2:
        bot.send_message(chat_id=uid, text='Неправильный формат\nПример правильного:\n/sendsanta 12345 Я люблю картошку',
                         parse_mode=telegram.ParseMode.HTML)
        return
    gid = args[0]
    mes_left = send_mes_check(uid, gid)
    if mes_left > 0:
        santa_message = ''
        for w in args[1:]:
            santa_message += w
            santa_message += ' '
        santa_message = convert_link(santa_message)
        print('Santa mes:', santa_message)
        gname = sql_get_group_name(gid)
        if gname is None:
            bot.send_message(chat_id=uid, text='Неправильный ID группы')
        else:
            santaid = findsanta(uid, gid)
            if santaid is None:
                bot.send_message(chat_id=uid, text='Не нашел твоего Санту. Наверное, распределения еще не было.')
            else:
                try:
                    bot.send_message(chat_id=santaid, text='📬  Твой подопечный из группы %s прислал сообщение:\n%s\nДля ответа '
                                                       'введи команду\n📤\n/sendgoodboy %s твой ответ' % (gname, santa_message, gid),
                                 parse_mode=telegram.ParseMode.HTML)
                    bot.send_message(chat_id=uid, text='Отправил')
                except telegram.error.Unauthorized:
                    bot.send_message(chat_id=uid, text='Упс, ошибка. Твой получатель меня заблокировал :(')
    else:
        act_plan = sql_get_group_info(0, 'plan', gid=gid)
        upgrade_keyboard = [[InlineKeyboardButton("✨ Улучшить", callback_data='upgrade:%s:%s' % (uid, gid))]]
        reply_markup = telegram.InlineKeyboardMarkup(upgrade_keyboard)
        t = '📵 Ой-ой! 📵\nТы достиг своего предела анонимных сообщений в группе %s!\n' \
            'Максимум ты мог отправить %s анонимок, они кончились\n' \
            'Что делать? Надо улучшить группу, это может сделать любой участник на странице группы.\n' \
            'Или нажми кнопку Улучшить под этим сообщением\n\n' \
            'Когда будет выбран вариант апгрейда, все пользователи, Санты и подопечные сразу смогут продолжить диалог ' \
            'без риска деанонимизации (или деСантоСекретизации?).\n\n' \
            'Возвращайся скорее!' % (sql_get_group_name(gid), plans[act_plan]['max_messages'])
        bot.send_message(chat_id=uid, text=t, reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML)



def notify_deleted_group(userlist, gname):
    bot = updater.bot
    for u in userlist:
        sleep(0.1)
        try:
            bot.send_message(chat_id=u,
                             text="Упс.\nКажется, администратор вашей группы <b>%s</b> был настоящим Гринчем.\n"
                                  "Потому что он удалил группу.\n"
                                  "Группы больше нет, всё пропало, Нового года не будет!\n"
                                  "Ладно, ладно, шучу.\n"
                                  "Всегда можно создать новую группу с помощью команды /newgroup.\n"
                                  "И новогоднее чудо будет спасено!" % gname,
                             parse_mode=telegram.ParseMode.HTML)
        except telegram.error.Unauthorized:
            print('IAMBOT===Я заблокирован')


def react_inline(bot, update):
    query = update.callback_query
    print("Q|", query.data)
    logger.info(f'Q| {query.data}')
    uid = query.from_user['id']
    if ('cfn' in query.data) and (get_user_state(uid) == "2"):
        new_name = query.data.split(':')[1]
        if check_uniq_name(uid, new_name) == 0:
            bot.send_message(chat_id=uid, text="Упс, такое имя уже есть.")
            return
        set_user_name(uid, new_name)
        set_user_state(uid, "3")
        gname = sql_get_group_name(get_user_active_group(uid))
        text_all = ['⛄️ Пополнение в группе <b>%s</b>\n<b>%s</b> врывается в чат' % (gname, new_name),
                    'На упряжке оленей к нам в группу <b>%s</b> примчался <b>%s</b>' % (gname, new_name),
                    'По улице бегал и пытался дарить сомнительные подарки прохожим <b>%s</b>, мы забрали его к себе в <b>%s</b>' % (
                    new_name, gname),
                    'Совершенно голый, но в чулках Санты к нам в зал собраний группы <b>%s</b> вошел <b>%s</b> и благородно сел на кресло у камина' % (
                    gname, new_name),
                    'Снеговик у крыльца вдруг ожил и превратился в <b>%s</b>, новогоднее чудо, чтоб быть одним из <b>%s</b>' % (
                    new_name, gname),
                    '<b>%s</b> вступил в партию <b>%s</b>. Они требуют у парламента продлить новогодние выходные на 3 месяца.' % (
                    new_name, gname),
                    'Мороз на окне образовал странный узор, оказавшийся надписью: <b>%s</b> с вами' % (new_name)]
        text_sel = text_all[random.randint(0, len(text_all) - 1)]
        print(text_sel)
        bot.send_message(chat_id=sql_get_group_info(uid, 'admin'), text=text_sel, parse_mode=telegram.ParseMode.HTML)
        empty_markup = telegram.InlineKeyboardMarkup([[]])
        text_name_conf = "Окаюшки, записал.\nА теперь напиши мне свои сокровенные мечты. Мб ты хочешь что-то особенное? " \
                         "Подскажи своему Санте, как тебя порадовать? Или просто передай привет, если хочешь получить сюрприз 🎁"
        bot.edit_message_text(text=text_name_conf, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=empty_markup)
    if ('showgroup' in query.data):
        gid = query.data.split(':')[1]
        act_plan = sql_get_group_info(0, 'plan', gid=gid)
        text = "<b>%s\n\n🔆 Описание</b>\n%s" % (sql_get_group_name(gid), sql_get_group_info(0, 'info', gid=gid))
        text = text + "\n\n📆 <b>Место и время</b>\n" + sql_get_group_info(0, 'time', gid=gid)
        if sql_get_group_info(0, 'open', gid) == "0":
            text = text + "\n\n🚷 Регистрация закрыта"
        else:
            text = text + "\n\nРегистрация открыта"
        text = text + "\n\nСсылка для регистрации\nhttps://t.me/tayniysantabot?start=%s" % (gid)
        text = text + "\n\n✳️ <b>Тариф</b> %s" % plans[act_plan]['name']

        ulist2 = get_group_users2(gid)
        text = text + "\n\n<b>👨‍👩‍👧‍👦 Участники</b> %s из %s" % (len(ulist2), plans[act_plan]['max_users'])
        new_in_users = 0
        for u in ulist2:
            text = text + "\n" + u['name']
            if u['name'] == 'new':
                new_in_users = 1
                if u['tg_username']:
                    text += f" (@{u['tg_username']})"
                elif str(uid) == str(u['uid']):
                    text += ' (это ты)'
        if new_in_users:
            text = text + "\n\nnew - пользователи, которые прошли по ссылке, но еще не написали Я буду хорошим Сантой " \
                          "и не выбрали себе имя. Можно потыкать их палочкой для ускорения или удалить через админку /adm перед " \
                          "распределением, без этого распределение запустить нельзя."
        groups_keyboard = []
        if sql_get_group_info(0, 'sorted', gid) == "1":
            goodboy = get_goodboy(uid, gid)
            text = text + "\n\nМои олени выбрали для тебя классного подопечного.\n"\
                          "👨‍🎤 Это <b>%s</b>, правда здорово?\n"\
                          "Однажды этот человек шепнул мне на ушко, что хочет:\n🧞‍♂️<i>%s</i>\n"\
                          "Но решение, конечно, за тобой.\n"\
                          "Кстати, вот где он будет в сроки вручения подарочков:\n<i>%s</i>\n"\
                          "Для отправки ему анонимного сообщения напиши:\n<code>/sendgoodboy %s текст сообщения</code>\n"\
                          "Для связи с Сантой:\n<code>/sendsanta %s сообщение</code>\n\n" \
                          % (get_user_name(goodboy, gid), get_user_info(goodboy, 0, gid), get_user_info(goodboy, 1, gid), gid, gid)
            #groups_keyboard.append([InlineKeyboardButton("Секретная почта", callback_data='mail:%s' % gid)])
        else:
            text += "\n\nЕсли захочешь что-то изменить в своих данных - имя, пожелание, адрес, то просто выйди из группы и заново пройди по " \
                    "ссылке, это легко сделать до распределения."
            text += "\nВыйти из группы можно, нажав кнопку <b>[Мои группы]</b>, выбери там группу и нажми Покинуть"
        if amiingroup(uid, gid):
            if plans[act_plan]['top'] == 0:
                groups_keyboard.append([InlineKeyboardButton("✨ Улучшить", callback_data='upgrade:%s:%s' % (uid, gid))])
            groups_keyboard.append([InlineKeyboardButton("Покинуть группу", callback_data='cancel:%s' % gid)])
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='mygroups')])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML, disable_web_page_preview=True)


    if ('cancel' in query.data):
        gid = query.data.split(':')[1]
        groups_keyboard = [[]]
        if sql_get_group_info(0, 'sorted', gid) == "0":
            text = "Ты уверен, что хочешь покинуть группу *" + sql_get_group_name(gid) + "*?\n" \
                   "Пока Тайных Сант не распределят, ты сможешь вернуться, заново пройдя по пригласительной ссылке."
            groups_keyboard.append([InlineKeyboardButton("Да, точно выйти", callback_data='confcanc:%s' % gid)])
        else:
            text = "Упс, выйти из распределенной группы уже нельзя. Можно будет выйти только если админ сперва отменит " \
                   "распределение Сант."
            groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='mygroups')])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    if ('confcanc' in query.data):
        gid = query.data.split(':')[1]
        groups_keyboard = [[]]
        del_user_from_group(uid, gid)
        text = "Ты покинул группу <b>" + sql_get_group_name(gid) + "</b>"
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML)


    if ('admgroup' in query.data):
        gid = query.data.split(':')[1]
        act_plan = sql_get_group_info(0, 'plan', gid=gid)
        # print("Показываю админку группы", get_group_name(gid))
        text = "<b>📟 Админка 📟️\n" + sql_get_group_name(gid) + "\n\nОписание</b>\n" + sql_get_group_info(0, 'info', gid=gid)
        text = text + "\n\n<b>Место и время</b>\n" + sql_get_group_info(0, 'time', gid=gid)
        groups_keyboard = [[]]
        if plans[act_plan]['top'] == 0:
            groups_keyboard.append([InlineKeyboardButton("✨ Улучшить", callback_data='upgrade:%s:%s' % (uid, gid))])
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admmain')])
        if sql_get_group_info(0, 'open', gid) == "0":
            text = text + "\n\n🚷 Регистрация закрыта"
            groups_keyboard.append([InlineKeyboardButton("Открыть регистрацию", callback_data='openclose:%s' % gid)])
        else:
            text = text + "\n\n🟢 Регистрация открыта"
            groups_keyboard.append([InlineKeyboardButton("Закрыть регистрацию", callback_data='openclose:%s' % gid)])
        if sql_get_group_info(0, 'sorted', gid) == "1":
            text = text + "\n♻️ Уже распределены Тайные Санты!"
            groups_keyboard.append([InlineKeyboardButton("Обнулить Тайных Сант", callback_data='del1santa:%s' % gid)])
            groups_keyboard.append([InlineKeyboardButton("Кто кому дарит?", callback_data='showsantas:%s' % gid)])
        else:
            text = text + "\n❔ Тайные Санты еще не распределены"
            groups_keyboard.append([InlineKeyboardButton("Распределить подарочки!", callback_data='sort:%s' % gid)])
        groups_keyboard.append([InlineKeyboardButton("Удалить группу", callback_data='delgrp:%s' % gid)])
        groups_keyboard.append([InlineKeyboardButton("Изменить описание", callback_data='editdesc:%s' % gid)])
        groups_keyboard.append([InlineKeyboardButton("Изменить место/время", callback_data='edittimeplace:%s' % gid)])
        text += "\n\nСсылка для регистрации\nhttps://t.me/tayniysantabot?start=%s" % (gid)
        text += "\n\n✳️ <b>Тариф</b> %s" % plans[act_plan]['name']
        ulist = get_group_users(gid)
        text += f"\n\n<b>👯‍♀️ Участники</b> {len(ulist)} из {plans[act_plan]['max_users']}"

        new_in_users = 0
        for i in ulist:
            if i == 'new':
                new_in_users = 1
            text = text + "\n" + i
        if new_in_users:
            text = text + "\n\nnew - пользователи, которые прошли по ссылке, но еще не написали Я буду хорошим Сантой " \
                          "и не выбрали себе имя. Можно потыкать их палочкой для ускорения или удалить через админку /adm перед " \
                          "распределением, без этого распределение запустить нельзя."
        if len(ulist):
            groups_keyboard.append([InlineKeyboardButton("Удалить участника", callback_data='delmember:%s' % gid)])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML,
                              disable_web_page_preview=True)
    if ('openclose' in query.data):
        gid = query.data.split(':')[1]
        if sql_get_group_info(0, 'open', gid=gid) == "0":  # если группа закрыта - открываем
            if str(sql_get_group_info(0, 'sorted', gid)) == "0":
                open_close_group(gid, "1")
                text = "Ура, регистрация снова открыта!"
            else:
                text = 'Нельзя открыть распределенную группу. Сперва нужно обнулить тайных сант.'
        else:
            open_close_group(gid, "0")
            text = "Кто не успел, я не виноват. Группа закрыта."
        groups_keyboard = [[InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)]]
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML)
    if ('sort' in query.data):
        gid = query.data.split(':')[1]
        text = "Ты уверен? Больше никого не ждем?\nПотом добавить опоздавших будет нельзя, " \
               "только перераспределять заново."
        glist = get_group_users(gid)
        groups_keyboard = []
        if "new" in glist:
            text = text + "\n\nВнимание! Кто-то не закончил регистрацию, я обнаружил имя NEW в списке.\n" \
                          "Они не написали Я буду хорошим Сантой или не ввели имя.\n" \
                          "Или попроси друзей закончить регистрацию, или удали их из группы."
        else:
            groups_keyboard.append([InlineKeyboardButton("Да", callback_data='srtconf:%s' % gid)])
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML)
    if ('srtconf' in query.data):
        gid = query.data.split(':')[1]
        mid = query.message.message_id
        if str(sql_get_group_info(0, 'sorted', gid)) == "0":    # проверка на отсортированность
            if len(get_group_users(gid, ineedid=True)) < 2:
                bot.send_message(chat_id=query.message.chat_id, text="Мало участников. Пригласи друзей!")
                return 0
            arrange(bot, gid, uid, mid, query)
        else:
            logger.info(f'Событие 13 поймано, группа {gid}')
            query.answer(text='Распределение уже запущено, идёт жеребьевка и рассылка сообщений')


    if ('del1santa' in query.data):
        gid = query.data.split(':')[1]
        act_plan = sql_get_group_info(0, 'plan', gid=gid)
        if plans[act_plan]['can_resort'] == 0:
            bot.send_message(chat_id=query.message.chat_id, text="Для перераспределения Сант нужно улучшить группу 🦾🎅🏾")
            return 0
        text = "Ты уверен? Все Тайны Санты отвяжутся от своих подопечных. Новое распределение будет совсем другим."
        groups_keyboard = [[InlineKeyboardButton("Да", callback_data='delsantaconf:%s' % gid)]]
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    if ('delsantaconf' in query.data):
        gid = query.data.split(':')[1]
        text = "Ок, всё забыл"
        del_santas(gid)
        set_group_sorted(gid, "0")
        groups_keyboard = [[]]
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    if ('delgrp' in query.data):
        gid = query.data.split(':')[1]
        text = "Ты уверен? Это нельзя будет отменить."
        groups_keyboard = [[InlineKeyboardButton("Да", callback_data='delgroupconf:%s' % gid)]]
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    if ('delgroupconf' in query.data):
        gid = query.data.split(':')[1]
        text = "Ок, всё удалил"
        userlist = get_group_users(gid, ineedid=True)
        t_del = PokaTimer(1, notify_deleted_group, userlist, gid)
        t_del.start()

        del_group(gid)
        groups_keyboard = [[]]
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    if ('delmember' in query.data):
        gid = query.data.split(':')[1]
        text = "Я сделал кнопку для каждого участника. Осторожно, дальше предупреждений не будет, участник будет " \
               "удален сразу."
        userlist = get_group_users(gid, ineedid=True)
        groups_keyboard = [[]]
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        for u in userlist:
            uname = get_user_name(u, gid)
            groups_keyboard.append([InlineKeyboardButton(uname, callback_data='delmmb:%s:%s' % (gid, u))])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)



    if ('delmmb' in query.data):
        gid = query.data.split(':')[1]
        uid = query.data.split(':')[2]

        del_user_from_group(uid, gid)
        try:
            bot.send_message(chat_id=uid, text="Администратор группы <b>%s</b> удалил тебя из неё.\n" % (sql_get_group_name(gid)),
                             parse_mode=telegram.ParseMode.HTML)
        except telegram.error.Unauthorized:
            pass
        text = "Готово! Удалим кого-нибудь еще?\n" \
               "Я сделал кнопку для каждого участника. Осторожно, дальше предупреждений не будет, участник будет " \
               "удален сразу."
        userlist = get_group_users(gid, ineedid=True)
        groups_keyboard = [[]]
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        for u in userlist:
            uname = get_user_name(u, gid)
            groups_keyboard.append([InlineKeyboardButton(uname, callback_data='delmmb:%s:%s' % (gid, u))])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        sleep(0.1)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)


    if ('editdesc' in query.data):
        gid = query.data.split(':')[1]
        uid = query.message.chat_id
        act_plan = sql_get_group_info(0, 'plan', gid=gid)
        if act_plan == 'plan1':
            text = 'Эта функция доступна на улучшенных тарифах'
        else:
            set_user_state(uid, "20", gid=gid)
            set_user_active_group(uid, gid)
            text = 'Пришли мне новое описание'
        bot.edit_message_text(text=text, chat_id=uid, message_id=query.message.message_id)


    if ('edittimeplace' in query.data):
        gid = query.data.split(':')[1]
        uid = query.message.chat_id
        act_plan = sql_get_group_info(0, 'plan', gid=gid)
        if act_plan == 'plan1':
            text = 'Эта функция доступна на улучшенных тарифах'
        else:
            set_user_state(uid, "21", gid=gid)
            set_user_active_group(uid, gid)
            text = 'Пришли мне новые детали вашей вечериночки 🤹'
        bot.edit_message_text(text=text, chat_id=uid, message_id=query.message.message_id)


    if ('mygroups' in query.data):
        sh_grps(uid, bot, query=query)

    if ('admmain' in query.data):
        adm_grps(uid, bot, query=query)

    if ('pay1' in query.data):
        uid = query.data.split(':')[1]
        link = create_payment('150.00', uid, '0', 0)
        bot.send_message(chat_id=uid, text='Твоя личная суперссылка\n%s' % link)
    if ('pay2' in query.data):
        uid = query.data.split(':')[1]
        link = create_payment('270.00', uid, '0', 0)
        bot.send_message(chat_id=uid, text='Твоя личная суперссылка\n%s' % link)
    if ('pay3' in query.data):
        uid = query.data.split(':')[1]
        link = create_payment('450.00', uid, '0', 0)
        bot.send_message(chat_id=uid, text='Твоя личная суперссылка\n%s' % link)
    if ("qiwipaypal" in query.data):
        bot.send_message(chat_id=uid, text='Твои личные суперссылки\n'
                                           'https://qiwi.com/p/79269166273\n'
                                           'https://paypal.me/aipunk')
    if ('payplan231' in query.data):
        uid = query.data.split(':')[1]
        gid = query.data.split(':')[2]
        link = create_payment('%s.00' % plans['plan231']['price'], uid, gid, 'plan231')
        bot.send_message(chat_id=uid, text=f'Твоя личная суперссылка для улучшения группы {sql_get_group_name(gid)}\n'
                                           f'{link}\n\nИногда нужно несколько минут для '
                                           f'активации тарифа.\n\nПроблема при оплате из Казахстана и других стран?\nВы блогер со своей аудиторией? Пишите на @aipunkbot')

    if ('payplan232:' in query.data):
        uid = query.data.split(':')[1]
        gid = query.data.split(':')[2]
        link = create_payment('%s.00' % plans['plan232']['price'], uid, gid, 'plan232')
        bot.send_message(chat_id=uid,
                         text=f'Твоя личная суперссылка для улучшения группы {sql_get_group_name(gid)}\n{link}\n\nИногда нужно несколько минут для '
                              f'активации тарифа.\n\nПроблема при оплате из Казахстана и других стран?\nВы блогер со своей аудиторией? Пишите на @aipunkbot')

    if ('payplan232(2):' in query.data):
        uid = query.data.split(':')[1]
        gid = query.data.split(':')[2]
        deltaprice = plans['plan232']['price'] - plans['plan231']['price']
        discount = plans['plan231']['price']
        link = create_payment('%s.00' % deltaprice, uid, gid, 'plan232')
        text = f'Для вашей группы действует скидка {discount}₱! 🎁\n\n' \
               f'Проблемы с оплатой из Казахстана и других стран? Пишите в бот техподдержки @aipunkbot\n\n' \
               f'Твоя личная суперссылка для улучшения группы {sql_get_group_name(gid)}\n{link}\n\nИногда нужно ' \
               f'несколько минут для активации тарифа'
        bot.send_message(chat_id=uid, text=text)

    if ('payplan233:' in query.data):
        uid = query.data.split(':')[1]
        gid = query.data.split(':')[2]
        link = create_payment('%s.00' % plans['plan233']['price'], uid, gid, 'plan233')
        bot.send_message(chat_id=uid,
                         text=f'Твоя личная суперссылка для улучшения группы {sql_get_group_name(gid)}\n{link}\n\nИногда нужно несколько минут для '
                              f'активации тарифа.\n\nПроблема при оплате из Казахстана и других стран?\nВы блогер со своей аудиторией? Пишите на @aipunkbot')

    if ('payplan233(2):' in query.data):
        uid = query.data.split(':')[1]
        gid = query.data.split(':')[2]
        deltaprice = plans['plan233']['price'] - plans['plan231']['price']
        discount = plans['plan231']['price']
        link = create_payment('%s.00' % deltaprice, uid, gid, 'plan233')
        text = f'Для вашей группы действует скидка {discount}₱! 🎁\n\n' \
               f'Проблемы с оплатой из Казахстана и других стран? Пишите в бот техподдержки @aipunkbot\n\n' \
               f'Твоя личная суперссылка для улучшения группы {sql_get_group_name(gid)}\n{link}\n\nИногда нужно ' \
               f'несколько минут для активации тарифа'
        bot.send_message(chat_id=uid, text=text)

    if ('payplan233(3):' in query.data):
        uid = query.data.split(':')[1]
        gid = query.data.split(':')[2]
        deltaprice = plans['plan233']['price'] - plans['plan232']['price']
        discount = plans['plan232']['price']
        link = create_payment('%s.00' % deltaprice, uid, gid, 'plan233')
        text = f'Для вашей группы действует скидка {discount}₱! 🎁\n\n' \
               f'Проблемы с оплатой из Казахстана и других стран? Пишите в бот техподдержки @aipunkbot\n\n' \
               f'Твоя личная суперссылка для улучшения группы {sql_get_group_name(gid)}\n{link}\n\nИногда нужно ' \
               f'несколько минут для активации тарифа'
        bot.send_message(chat_id=uid, text=text)

    if ('upgrade' in query.data):
        uid = query.data.split(':')[1]
        gid = query.data.split(':')[2]
        act_plan = sql_get_group_info(0, 'plan', gid=gid)
        t = 'Возможности группы <b>%s</b> можно расширить\n\n' \
            'Куда Санта будет класть подарки, под какое дерево?\n\n' % sql_get_group_name(gid)
        t = 'Возможности группы <b>%s</b> можно расширить\n\n' \
            'Куда Санта будет класть подарки, под какое дерево?\n\n' % sql_get_group_name(gid)
        ab = sql_ab(uid)
        for k in ('plan1', 'plan231', 'plan232', 'plan233'):
            if plans[k]['active'] == 1:
                if ab == 'A':
                    t += str('<b>%s</b>\n%s\n💰 %s₽\n\n' % (plans[k]['name'], plans[k]['desc'], plans[k]['price']))
                elif ab == 'B':
                    t += str('<b>%s</b>\n%s\n\n' % (plans[k]['name'], plans[k]['desc']))
        t += 'Любой участник группы может активировать дополнительный функционал\n\n'
        pay_keyboard = [[]]
        if act_plan in (
                'plan1', 'plan2', 'plan3', 'plan4', 'plan12', 'plan13'):  # если бесплатная или прошлогодняя группа
            pay_keyboard.append(
                [InlineKeyboardButton(plans['plan231']['name'], callback_data='payplan231:%s:%s' % (uid, gid))])
            pay_keyboard.append(
                [InlineKeyboardButton(plans['plan232']['name'], callback_data='payplan232:%s:%s' % (uid, gid))])
            pay_keyboard.append(
                [InlineKeyboardButton(plans['plan233']['name'], callback_data='payplan233:%s:%s' % (uid, gid))])
        elif act_plan == 'plan231':  # если первый тариф - делаем промежуточную скидку для повышения
            pay_keyboard.append(
                [InlineKeyboardButton(plans['plan232']['name'], callback_data='payplan232(2):%s:%s' % (uid, gid))])
            pay_keyboard.append(
                [InlineKeyboardButton(plans['plan233']['name'], callback_data='payplan233(2):%s:%s' % (uid, gid))])
        elif act_plan == 'plan232':  # если второй тариф - делаем промежуточную скидку для повышения
            pay_keyboard.append(
                [InlineKeyboardButton(plans['plan233']['name'], callback_data='payplan233(3):%s:%s' % (uid, gid))])
        elif act_plan in ('plan233'):
            t += 'Эта группа уже максимально прокачана!'
        t += '\n\nХотите своего корпоративного бота с уникальными текстами? Пишите нам! Работаем с юрлицами. @aipunkbot'
        # pay_keyboard.append([InlineKeyboardButton('Назад', callback_data='showgroup:gid')])
        reply_markup = telegram.InlineKeyboardMarkup(pay_keyboard)
        bot.send_message(chat_id=uid, text=t, reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML)

    if ('showsanta' in query.data):
        # bot.send_message(chat_id=query.message.chat_id,
        #                  text="Функция скоро заработает")
        # return 0
        gid = query.data.split(':')[1]
        act_plan = sql_get_group_info(0, 'plan', gid=gid)
        if plans[act_plan]['can_see_all_santas'] == 0:
            bot.send_message(chat_id=query.message.chat_id,
                             text="Для этого нужно улучшить группу 🦾🎅🏾")
            return 0
        else:
            res = get_all_goodboys(gid)
            if res:
                bot.send_message(chat_id=uid, text=res)
            else:
                bot.send_message(chat_id=uid, text='Не нашел группу или Санты не распределены')

    if 'reg_step2' in query.data:
        gid = query.data.split(':')[1]
        t = "Когда зарегистрируются все участники, я проведу волшебную жеребьёвку и тебе достанется " \
        "подопечный, которому к оговоренной дате ты должен будешь подготовить классный подарок 🎁 \n" \
        "А кто-то станет твоим личным Тайным Сантой и подготовит подарочек специально для тебя."
        rm = InlineKeyboardMarkup([[InlineKeyboardButton("Поскорее бы", callback_data=f'reg_step3:{gid}')]])
        bot.edit_message_text(text=t, chat_id=uid, message_id=query.message.message_id, reply_markup=rm,
                              parse_mode=telegram.ParseMode.HTML)


    if 'reg_step3' in query.data:
        gid = query.data.split(':')[1]
        t = "Тут всё серьезно, поэтому, чтобы показать свои серьёзные намерения, напиши мне фразу\n" \
        "<code>Я буду хорошим Cантой</code>\nКопипастить нельзя, я узнаю!\n(и да, не забывай про большие буквы)"
        bot.edit_message_text(text=t, chat_id=uid, message_id=query.message.message_id, reply_markup='',
                              parse_mode=telegram.ParseMode.HTML)


def echo(bot, update):
    uid = update.message.from_user.id
    tg_username = update.message.from_user.username
    user_state = get_user_state(uid)

    print(f"U| {tg_username}: ", update.message.text, update.message.message_id)
    # bot.edit_message_reply_markup(uid, message_id=update.message.message_id-1, reply_markup='')
    if uid < 0:
        return
    if (update.message.text == "300"):
        bot.send_message(chat_id=uid, text="Ты сам знаешь, что делать")

    elif update.message.text == "Мои группы":
        sh_grps(uid, bot, update)

    elif update.message.text == "Управление":
        adm_grps(uid, bot, update=update)

    elif ((("я буду хорошим сантой" in update.message.text.lower())
           or ("I love cat's balls" in update.message.text))
          and (user_state == "1")):
        text = "Молодец!\nТеперь напиши своё имя для этой группы.\nПостарайся придумать такое, чтоб любой Санта мог " \
               "понять, что ты - это ты. Если у вас на работе 8 Ярополков, то стоит добавить фамилию, чтоб не " \
               "запутать своего будущего Санту.\nЕсли дома мама зовёт тебя 'Пельмешечка', то не стоит так " \
               "подписываться в группе друзей"
        bot.send_message(chat_id=uid, text=text)
        set_user_state(uid, "2")
    elif (user_state == "2"):
        new_name = update.message.text
        if len(new_name) > 25:
            bot.send_message(chat_id=uid, text="Очень длинное имя\nДавай покороче, а?")
            return
        if any(s in new_name for s in bad_symbols):
            bot.send_message(chat_id=uid, text="Нельзя использовать символы %s" % bad_symbols)
            return
        if check_uniq_name(uid, new_name) == 0:
            bot.send_message(chat_id=uid, text="Упс, такое имя уже есть.")
            return
        else:
            confirm_name_keyboard = [[InlineKeyboardButton("Да", callback_data='cfn:%s' % new_name)]]
            reply_markup = telegram.InlineKeyboardMarkup(confirm_name_keyboard)
            bot.send_message(text="*%s* - это твое имя?\nМне запомнить тебя так?\nЕсли все ок, просто нажми Да.\n"
                                  "Если тебе не нравится это имя, просто введи новое" % new_name,
                                  chat_id=uid, message_id=update.message.message_id,
                                  reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    elif (user_state == "3"):
        # new_info = convert_link(update.message.text)
        new_info = update.message.text
        if any(s in new_info for s in bad_symbols):
            bot.send_message(chat_id=uid, text="Нельзя использовать символы %s\nЕсли хочешь использовать "
                                               "ссылку, в которой есть такой символ, воспользуйся сервисом "
                                               "для замены ссылки\nhttp://goo.su" % bad_symbols)
            return
        set_user_info(uid, 0, new_info)
        set_user_state(uid, "4")
        bot.send_message(text="Вот так (или вот где) будет проходить ваше мероприятие:\n"
                              "<b>%s</b>\n\n"
                              "Как тебя найти моим оленям?\n"
                              "Подтверди, что ты придешь, если у вас живая встреча или напиши куда лететь моим почтовым квадрокоптерам."
                              % sql_get_group_info(uid, 'time'),
                         chat_id=uid, message_id=update.message.message_id,
                         parse_mode=telegram.ParseMode.HTML)

    elif (user_state == "4"):
        new_info = update.message.text
        if any(s in new_info for s in bad_symbols):
            bot.send_message(chat_id=update.message.chat_id, text="Нельзя использовать символы %s" % bad_symbols)
            return
        set_user_info(uid, 1, new_info)
        set_user_state(uid, "5")
        custom_keyboard = [['Мои группы'], ['Управление']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        bot.send_message(text="Ура, <b>%s!</b>\n"
                              "Регистрация завершена\n"
                              "Если дальше захочешь что-нибудь поправить в личных данных, например имя или пожелание "
                              "подарков, можно будет покинуть группу и пройти регистрацию заново.\n\n"
                              "Когда все ваши друзья присоединятся, админ группы запустит распределение и я пришлю тебе"
                              " имя счастливчика. Правда, здорово?\n<i>Можешь не отвечать, я робот, мне всё равно</i>"
                              "\n\nНовости наших ботов:\nt.me/aipunk_news'"
                              % get_user_name(uid),
                         chat_id=update.message.chat_id, message_id=update.message.message_id,
                         reply_markup=reply_markup, parse_mode=telegram.ParseMode.HTML)

    elif (user_state == "10"):
        new_info = update.message.text
        if any(s in new_info for s in bad_symbols):
            bot.send_message(chat_id=update.message.chat_id, text="Нельзя использовать символы %s" % bad_symbols)
            return
        set_group_info(uid, 0, new_info)
        bot.send_message(text="Хорошо. А теперь время написать когда и где планируется обмен подарками.",
                         chat_id=update.message.chat_id) #message_id=update.message.message_id)
        set_user_state(uid, "11")

    elif (user_state == "20"):
        new_info = update.message.text
        set_group_info(uid, 0, new_info)
        bot.send_message(text="Починил!", chat_id=uid) #, message_id=update.message.message_id)
        user_not_in_grp = addusertodb(uid, get_user_active_group(uid), check=1, tg_username=tg_username)
        if user_not_in_grp == 1:
            set_user_state(uid, "1")
        elif user_not_in_grp == 0:
            set_user_state(uid, "5")

    elif (user_state == "11"):
        new_info = update.message.text
        if any(s in new_info for s in bad_symbols):
            bot.send_message(chat_id=update.message.chat_id, text="Нельзя использовать символы %s" % bad_symbols)
            return
        set_group_info(uid, 1, new_info)
        bot.send_message(text="Ура, получилось!\nВот тебе ссылка:\nПерешли ее своим друзьям.\n"
                              "Не забудь сам пройти по ней и ответить на вопросы.\n"
                              "https://t.me/tayniysantabot?start=%s\n\nЕсли кто-то пользуется веб-версией телеграма, ему"
                              " для старта нужно ввести команду /start %s"
                              % (get_user_active_group(uid), get_user_active_group(uid)),
                         chat_id=uid, message_id=update.message.message_id, disable_web_page_preview=True)
        set_user_state(uid, "1")

    elif (user_state == "21"):
        new_info = update.message.text
        set_group_info(uid, 1, new_info)
        bot.send_message(text="Починил!", chat_id=uid)#, message_id=update.message.message_id)
        user_not_in_grp = addusertodb(uid, get_user_active_group(uid), check=1, tg_username=tg_username)
        if user_not_in_grp == 1:
            set_user_state(uid, "1")
        elif user_not_in_grp == 0:
            set_user_state(uid, "5")

    else:
        bot.send_message(chat_id=uid, text="Аушки? Или ты что-то пишешь неправильно, "
                                           "или я сейчас вообще не жду от тебя текст")


def do_hour_tick(bot, ticktime):
    if ticktime == 0:
        # если запускаемся первый раз
        dt_now = datetime.now()
        next_time = (stat_period / 60 - dt_now.minute) * 60
        print('Отчет через %i минут' % (next_time/60))
        bot.send_message(chat_id=admin_list[0], text='Отчет через %i минут' % (next_time/60))
    else:
        users_now = len(get_all_users())
        conn = sqlite3.connect("mydb.ext")
        cursor = conn.cursor()
        cursor.execute("""SELECT userid, registration_date FROM ids where registration_date is not null""")
        dt = datetime.utcnow()
        t = ''
        fresh_users = []
        for c in cursor.fetchall():
            reg = datetime.strptime(c[1], "%d.%m.%Y %H:%M")
            if (dt - timedelta(hours=1)) < reg:
                t += '%s %s\n' % (c[0], c[1])
                fresh_users.append(c[0])
        if len(fresh_users):
            bot.send_message(chat_id=admin_list[0], text='*** Cтатистика ***\nНовых юзеров: %s (%s)\n' #\nСкорость: %.0f в час'
                                                     % (len(fresh_users), users_now)) #, users_delta/ticktime*60*60))
        next_time = stat_period
    t0 = PokaTimer(next_time, do_hour_tick, bot, next_time)
    t0.start()
    # print('tick')



def stat(bot, update, args):
    # users_now = len(get_all_users())
    if len(args):
        if args[0] in ('d', 'D'):
            t = 'D'
    else:
        t = 'H'
    conn = sqlite3.connect("mydb.ext")

    import pandas as pd
    df = pd.read_sql_query("""SELECT userid, registration_date FROM ids where registration_date is not null""", conn)
    df['reg'] = pd.to_datetime(df.registration_date)
    # df.resample('H', on='reg').count()

    df = df[(df['reg'] > '2022-11-01') & (df['reg'] < '2022-12-31')]
    print(df)
    df2 = df.resample('%s' % t, on='reg')['reg'].count()
    df3 = pd.DataFrame(df2)
    if t == 'H':
        df3.index = df3.index.hour
    else:
        df3.index = df3.index.day
    fig = df3['reg'].plot(kind='bar', figsize=(20, 16), fontsize=10).get_figure()
    fig.savefig('test.png')

    bot.send_photo(chat_id=admin_list[0], photo=open('test.png', 'rb'))


def backup(bot, update):
    dt_now = datetime.now()
    new_filepath = dt_now.strftime('backups/mydb_backup%d%m%y.ext')
    import shutil
    shutil.copy2('mydb.ext', new_filepath)
    bot.send_document(admin_list[0], open(new_filepath, 'rb'))
    t_backup = PokaTimer(60*60*24, backup, updater.bot, '')
    t_backup.start()


def secret(bot, update, args):
    uid = update.message.chat_id
    if len(args):
        res = get_all_goodboys(args[0])
        if res:
            bot.send_message(chat_id=uid, text=res)
    else:
        bot.send_message(chat_id=uid, text='Не нашел группу или Санты не распределены')


if __name__ == '__main__':


    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler("start", start, pass_args=True))
    dispatcher.add_handler(CommandHandler("pay", pay))
    dispatcher.add_handler(CommandHandler("checkplan", check_plan, pass_args=True))
    dispatcher.add_handler(CommandHandler("setplan", set_plan, pass_args=True))
    dispatcher.add_handler(CommandHandler("300", trista, pass_args=True))
    dispatcher.add_handler(CommandHandler("help", help_com))
    dispatcher.add_handler(CommandHandler("adm", admin))
    dispatcher.add_handler(CommandHandler("spam_admins", spam_admin, pass_args=True))
    dispatcher.add_handler(CommandHandler("newgroup", newgroup, pass_args=True))
    dispatcher.add_handler(CommandHandler("thanks", thanks))
    dispatcher.add_handler(CommandHandler("gogogo", gogogo))
    dispatcher.add_handler(CommandHandler("gogogo2", gogogo2))
    dispatcher.add_handler(CommandHandler("mesact", mes_act))
    dispatcher.add_handler(CommandHandler("sendgoodboy", sendgoodboy, pass_args=True))
    dispatcher.add_handler(CommandHandler("sendsanta", sendsanta, pass_args=True))
    dispatcher.add_handler(CommandHandler("touser", senduser, pass_args=True))
    dispatcher.add_handler(CommandHandler("check", sendcheck, pass_args=True))
    dispatcher.add_handler(CommandHandler("ch2", ch2, pass_args=True))
    dispatcher.add_handler(CommandHandler("stat", stat, pass_args=True))
    dispatcher.add_handler(CommandHandler("stata", stata, pass_args=True))
    dispatcher.add_handler(CommandHandler("secret", secret, pass_args=True))
    dispatcher.add_handler(CommandHandler("conv", check_conv, pass_args=True))
    dispatcher.add_handler(CommandHandler("fg", find_group_by_name, pass_args=True))
    dispatcher.add_handler(CommandHandler("sg", show_group_to_me, pass_args=True))
    dispatcher.add_handler(CommandHandler("backup", backup))
    dispatcher.add_handler(CommandHandler("ads", ads, pass_args=True))

    dispatcher.add_handler(MessageHandler(Filters.text, echo))
    dispatcher.add_handler(CallbackQueryHandler(react_inline))

    updater.start_polling()

    do_tick(updater.bot, 'lalalala')
    # do_hour_tick(updater.bot, 0) # статистика

    # t_backup = PokaTimer(60*60*24, backup, updater.bot, '')
    # t_backup = PokaTimerAlarm('00:00', backup, updater.bot, '')
    # t_backup.start()

    print('start', botname)
