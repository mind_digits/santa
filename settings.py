from telegram.ext import Updater
import httplib2
from oauth2client.service_account import ServiceAccountCredentials
from classes import *

test = 0
# test = 1

if test:
    updater = Updater(token="328682751:AAHDnN7m4j_Ur_9Nb9DNGi-exuy2hQhpXZ4")  # , use_context=False) #test
    # groupid = '-390912564' #тестовая
    botname = 'cami_buddy_bot'
else:
    updater = Updater(token="1057191621:AAFuZTOPlxIpq-B4ilANQ6SAmonGcEALGFw")  # , use_context=False)  # tayniysantabot
    # groupid = '-1001343520717'  # продакшн
    botname = 'tayniysantabot'

admin_list = ['182116723']

# bad_symbols = "#*_'`><+=[]{}"
bad_symbols = "><"

games = Games()
users = Users()
mes_with_kb = {}

token_yookassa = 'live_5EP28QezuzJfZAvh2rR6tF_CBN067nF3byoGxOVuPyA'
shop_id = 854689

stat_period = 60 * 60 * 6
users_then = 0

plans = {
    'plan1': {
        'name': '🌵 Сосна',
        'max_users': 6,
        'max_messages': 3,
        'price': 0,
        'active': 1,
        'desc': "Базовый, 6 пользователей и 3 анонимных сообщения на каждого",
        'top': 0,
        'can_see_all_santas': 0,
        'can_resort': 0,
    },
    'plan231': {
        'name': '🌲 Ёлочка',
        'max_users': 16,
        'max_messages': 100,
        'price': 330,
        'active': 1,
        'desc': "Основной, 16 пользователей, 100 анонимных сообщений, возможность перераспределять Тайных Сант",
        'top': 0,
        'can_see_all_santas': 0,
        'can_resort': 1,
    },
    'plan232': {
        'name': '🎄 Норвежская пихта',
        'max_users': 30,
        'max_messages': 10000,
        'price': 850,
        'active': 1,
        'desc': "Расширенный, для больших групп до 30 человек, с полным функционалом\n"
                "Администратор группы может узнать, кто был чьим Сантой в случае неполучения подарка\n"
                "📟 Работает техподдержка",
        'top': 0,
        'can_see_all_santas': 1,
        'can_resort': 1,
    },
    'plan233': {
        'name': '🧑‍🚀 Корпоративный PRO',
        'max_users': 500,
        'max_messages': 10000,
        'price': 2990,
        'active': 1,
        'desc': "Премиум, для неограниченных групп, полный функционал\n"
                "Популярен среди организаций\n⭐️ Приоритетная техподдержка",
        'top': 1,
        'can_see_all_santas': 1,
        'can_resort': 1,
    },
    'plan12': {  # архивный
        'name': '🌲 Ёлочка (архивный)',
        'max_users': 16,
        'max_messages': 100,
        'price': 330,
        'active': 0,
        'desc': "Основной, 16 пользователей, 100 анонимных сообщений, возможность перераспределять Тайных Сант",
        'top': 0,
        'can_see_all_santas': 0,
        'can_resort': 1,
    },
    'plan13': {  # архивный
        'name': '🎄 Норвежская пихта (архивный)',
        'max_users': 90,
        'max_messages': 10000,
        'price': 850,
        'active': 0,
        'desc': "Расширенный, для самых больших групп до 90 человек, с полным функционалом\n"
                "Только тут администратор группы может узнать, кто был чьим Сантой в случае неполучения подарка\n"
                "Популярен среди организаций\n📟 Работает техподдержка",
        'top': 1,
        'can_see_all_santas': 1,
        'can_resort': 1,
    },
    'plan2': {  # архивный
        'name': '🌲 Ёлочка (очень архивный)',
        'max_users': 30,
        'max_messages': 100,
        'price': 190,
        'active': 0,
        'desc': "Основной, 30 пользователей, 100 анонимных сообщений, возможность перераспределять Тайных Сант",
        'top': 0,
        'can_see_all_santas': 0,
        'can_resort': 0,
    },
    'plan4': { # архивный
        'name': '🌲 Ёлочка (архивный)',
        'max_users': 20,
        'max_messages': 100,
        'price': 290,
        'active': 0,
        'desc': "Основной, 20 пользователей, 100 анонимных сообщений, возможность перераспределять Тайных Сант",
        'top': 0,
        'can_see_all_santas': 0,
        'can_resort': 0,
    },
    'plan3': { # архивный
        'name': '🎄 Норвежская пихта (архивный)',
        'max_users': 90,
        'max_messages': 10000,
        'price': 690,
        'active': 0,
        'desc': "Расширенный, для самых больших групп до 90 человек, с полным функционалом\n"
                  "Только тут администратор группы может узнать, кто был чьим Сантой в случае неполучения подарка\n"
                  "Популярен среди организаций\n📟 Работает техподдержка",
        'top': 0,
        'can_see_all_santas': 1,
        'can_resort': 0,
    }
}

# logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("SantaAPP")
logger.setLevel(logging.INFO)
fh = logging.FileHandler("nov5.log")
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)
